yay -R xf86-video-nouveau
yay -S --needed bumblebee mesa-demos nvidia nvidia-settings nvidia-utils opencl-nvidia opencl-headers lib32-nvidia-utils lib32-opencl-nvidia

sudo systemctl enable --now bumblebeed.service
sudo gpasswd -a $USER bumblebee
